package com.ly.webStsart;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.ly.bean.spring.BeanFactory;
import com.ly.entity.DataConfig;
/**
 * 服务器启动加载所以Annotation的配置标签
 * @author admin
 *
 */

public class WebStartListender implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("系统已经销毁");

	}
/**
 * 初始化参数,加载下面各种注解数据
 */
	@Override
	public void contextInitialized(ServletContextEvent serlvetContext) {
		// TODO Auto-generated method stub
		ServletContext context = serlvetContext.getServletContext();
		BeanFactory factory = new BeanFactory();
		try {
			Map<String,List<String>> controlRequMap = factory.getAction();
			context.setAttribute("springMVC", controlRequMap);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	


}
