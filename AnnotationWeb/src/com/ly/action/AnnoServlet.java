package com.ly.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;

import com.ly.Annotation.Controller;
import com.ly.Annotation.RequestMapping;

/**
 * 封装servlet
 * 
 * @author admin
 * 
 */

public class AnnoServlet extends HttpServlet {
	private String url;
	private boolean isForward;

	public AnnoServlet() {
	}

	public AnnoServlet(String url) {
		this.url = url;
	}

	private static final long serialVersionUID = 5095842575011945856L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");
		Map<String, List<String>> controMethMap = (Map<String, List<String>>) getServletContext()
				.getAttribute("springMVC");
		String servletPath = request.getServletPath();
		String[] servletHead = this.getServerPath(servletPath);
		try {
			String url = initRequestMethod(servletHead, controMethMap, request);
			request.getRequestDispatcher(url).forward(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String initRequestMethod(String[] servletHead,
			Map<String, List<String>> controMethMap, HttpServletRequest request)
			throws Exception {
		Set<String> control = controMethMap.keySet();// action的全路径
		String returnPath = "";
		for (String controPath : control) {
			String[] pathArr= controPath.split("\\.");
			String pathHead = pathArr[pathArr.length - 1];
			if (servletHead[0].equals(pathHead)) {// 判断进入哪个controller
				Class clazz = Class.forName(controPath);
				Object action = clazz.newInstance();
				Method[] method = clazz.getMethods();
				for (Method meth : method) {
					if (meth.getName().equals(servletHead[1])) {// 初始化
						Method met = clazz.getMethod(servletHead[1],
								new Class[] { HttpServletRequest.class });
						returnPath = (String) met.invoke(action,
								new Object[] { request });
					}
				}

			}

		}
		return returnPath;
	}

	public String[] getServerPath(String servletPath) {
		servletPath = servletPath.substring(1, servletPath.length());
		String[] strArr = servletPath.split("\\.")[0].split("/");

		return strArr;
	}

	public String returnJumpURL(String classPath) throws Exception {
		Class clazz = Class.forName(classPath);
		Object obj = clazz.newInstance();
		Method method = clazz.getMethod("servletOne", new Class[] {});
		String url = (String) method.invoke(obj, new Object[] {});
		return url;
	}

	public static void main(String[] args) throws Exception {
//		Class clazz = Class.forName("com.ly.action.annoAction");
//		Object obj = clazz.newInstance();
//		Method method = clazz.getMethod("servletOne", new Class[] {});
//		String str = (String) method.invoke(obj, new Object[] {});
//		System.out.println(str);
		 String controPath="com.ly.action.annoAction";
		 String[] pathHead = controPath.split("\\.");
		 System.out.println(pathHead[pathHead.length-1]);
//		 for (String string : pathHead) {
//			System.out.println(string);
//		}
	}
}
