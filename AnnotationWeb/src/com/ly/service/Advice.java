package com.ly.service;

public interface Advice {
	public abstract void before();
	public abstract void after();
	public abstract void surround();
	public abstract void Exception();

}
