package com.ly.form;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public interface Action {

	public void excute(HttpServletRequest request,ActionForm form,Map<String,String> actionForm);
}
